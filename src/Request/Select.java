package Request;
import java.util.ArrayList;
import java.util.List;

import Files.Data;
import Filters.Condition; 

public class Select extends Transformation 
{    
	List<String> columns = new ArrayList<String>();  
	
	
	@Override
	public void execut() 
	{    
		for( Data data : this.getInputs() )
		{ 
			Data output = new Data(); 
			
			for( String col : columns )
			{  
				Integer idColumn = data.getColumns().get(col);
				 
				if(idColumn != null)
				{  
					// traitement...
					//conditions...
					output.getColumns().put(col, idColumn);
					output.getRows().put(idColumn, data.getRows().get(idColumn));
				} 
			} 

			this.getOutputs().add( output ); 
		}
		if(columns.size()==0) this.getOutputs().addAll(this.getInputs());
	}
 
	
	public void addColumn(String column) {
		this.columns.add(column);
	}


	@Override
	public void display() {
		for( Data data : this.getOutputs() )
		{ 
			data.display();
		}
	} 
}
