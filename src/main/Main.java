package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List; 

import Files.FichierCSV;
import Filters.Condition;
import Request.Combine;
import Request.Select;
import Request.Task;
 
public class Main  {
	 
	
	public static void main(String[] args) 
	{ 
		
		Task t1 = new FichierCSV("1.csv");  
		
		Select t2 = new Select(); 
		t2.setInputs( t1.getOutputs() ); 
		t2.addColumn("Sexe");
		t2.addColumn("Age");

		Select t3 = new Select(); 
		t3.setInputs( t2.getOutputs() );
		t3.addColumn("Age");   
		
		Runner runner1 = new Runner();
		runner1.addTask(t1);
		runner1.addTask(t2);
		runner1.addTask(t3);
		
		 
		
		
		Task t11 = new FichierCSV("1.csv");  
		
		Select t12 = new Select(); 
		t12.setInputs( t1.getOutputs() ); 
		t12.addColumn("Sexe");
		t12.addColumn("Age");

		Combine t13 = new Combine(); 
		t13.setInputs( t2.getOutputs() ); 
		Runner runner2 = new Runner();
		runner2.addTask(t11);
		runner2.addTask(t12); 
		
		
		
		try { 
			runner1.join();
			runner2.join(); 
		}
		catch (InterruptedException e) {}
		

		runner1.run();
		runner2.run();
	}
	
}
