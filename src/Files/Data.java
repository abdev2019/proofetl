package Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



public class Data 
{ 
	HashMap<Integer, ArrayList<String>> rows    = new HashMap<Integer, ArrayList<String>>();
	HashMap<String, Integer> 			columns = new HashMap<String, Integer>();
	
	public HashMap< String,Integer> getColumns() {
		return columns;
	}
	public HashMap<Integer, ArrayList<String>> getRows() {
		return rows;
	}
	
	public void display()
	{
		System.out.print("Columns : ");
		for(Map.Entry<String,Integer> entry : columns.entrySet()) {
			System.out.print(entry.getKey() +" [ ");

			for( String val : rows.get(entry.getValue()) ) {
				System.out.print(val +" ,");
			}
			
			System.out.println(" ] ");
		}
	}
}





