package Files;


import java.io.File; 
import java.io.FileReader;
import java.util.ArrayList;

import Request.Task;
import au.com.bytecode.opencsv.CSVReader;



public class FichierCSV extends Task implements IFichier
{ 
	String fileName; 
	Data data; 
	
	
	public FichierCSV() { 
	}
	
	public FichierCSV(String fileName) {
		this.fileName = fileName;
	}
	
	public File loadFile() {
		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(fileName));
			String [] nextLine;
			if((nextLine = reader.readNext()) != null)
			{
				data = new Data();
				for(int i=0; i<nextLine.length;i++)
				{
					data.rows.put(i, new ArrayList<String>());
					data.columns.put(nextLine[i].trim(),i);
				}
				
			    while ((nextLine = reader.readNext()) != null) 
			    { 
			    	for(int i=0; i<nextLine.length;i++)
					{
						data.rows.get(i).add( (nextLine[i].trim()) );
					}
			    }
			}
		} 
		catch (Exception e) {}
	    
		return null;
	} 
	
	public void setFilename(String filename) {
		this.fileName = filename;
	}
	
	public void display() {
		data.display();
	}
	
	public Data getData() {
		return data;
	}

	@Override
	public void execut() {
		// load data
		loadFile();
		this.getOutputs().add(data);
	}
 
}
